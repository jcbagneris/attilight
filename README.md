# Attilight Theme for Pelican

[![Travis build status](https://img.shields.io/travis/jcbagneris/attilight.svg)](https://travis-ci.org/jcbagneris/attilight)
[![Calendar versioning](https://img.shields.io/badge/calver-YYYY.0M.MINOR-22bfda.svg)](http://calver.org/)

A simple and responsive theme for [Pelican](https://github.com/getpelican/pelican).

The look is inspired by the [attila theme](https://github.com/arulrajnet/attila),
but nearly everything was modified or rewritten with the idea of removing many
features I would not use and simplifying templates and css as much as possible.

The main css uses flexbox: if for some reason you have to support legacy
browsers, you can switch to the no-flex branch. Note that no-flex is no longer
maintained: it "works" but will never change.

The icons are from [icomoon.io](https://icomoon.io/).

The stylesheet uses [normalize.css](http://necolas.github.io/normalize.css/).

## Install

The recommended way to install is to use a pre-built release and the
pelican-themes command. The stylesheets in these releases are adapted for fluxbox
correct rendering with [autoprefixer](https://github.com/postcss/autoprefixer) and minified.

* Download the latest built [release](https://github.com/jcbagneris/attilight/releases/latest)
* Decompress somewhere
* Then `pelican-themes -i <path-to-decompressed-attilight>`
* List all themes `pelican-themes -lv` to check it's correctly installed
* Put `THEME = 'attilight'` in your pelicanconf.py

Please note that in the built release, assets such as fonts and css are
versioned (like `style-ea82f8e.css`), allowing you to use aggressive caching
policies (`Cache-Control: max-age=31536000`): the name of a given version
is always unique (and of course, the base template is modified accordingly).

## Settings

Attilight makes its best to honor regular pelican settings.

Additional settings are described below.

### Open Graph ###

Open Graph meta properties are inferred from the `SITENAME` and
`SITEURL` settings.

In addition, `OG_LOCALE` is set to `en_US` by default, and `OG_DESCRIPTION` to
`AUTHOR`'s blog by default.

### Header style

To define a custom header background, either set `HEADER_BG_IMG` to
the url of a background image, or `HEADER_BG_COLOR` to any valid css color.
Note that the default text color in the header is white, thus the image or color
are expected to be rather dark (to contrast with the white background of the
main part of the page). If you insist on changing this color, set
`HEADER_FG_COLOR` to any valid css color.

If none of these settings is defined, `#003388` will be used as a header
background color.

But that's not the full story. You can also have a custom background for a
specific page or post. Just set a `header_bg_img` or `og_image` metadata to an
url or path to an image, and it will be used instead of the regular header
background. As for the standard header, images are supposed to be dark as the
title and subtitle will be white. If this does not please you, add a
`header_fg_color` to the metadata.

### Index page

The `SHOW_FULL_ARTICLE` setting allows to show full articles on the index page,
instead of summaries.


### External feed URL

You can specify an external feed URL (e.g. FeedBurner) in `SOCIAL` using the
`rss` or `rss-square` or `feed` icons. A `<link>` tag for the external feed will be
placed in `<head>` instead of the default Pelican feeds.

### Authors Bio

```python
AUTHORS_BIO = {
  "jd": {
    "fullname": "John Doe",
    "image": "https://johndoe.github.io/assets/images/avatar.png",
    "website": "http://blog.johndoe.net",
    "location": "Anywhere City",
    "bio": "Hi, my name is John Doe"
  }
}
```

## Code blocks and syntax highlighting

If you need line numbers in code blocks, use the `inline` mode for the `linenos`
option. If you use `table`, you have to style the table yourself.

## Internals

The templates are logically organized in 4 groups:

- lists of posts with summary: `index.html`, `author.html`, `category.html`, `tag.html`
  These templates are the only ones which support pagination.

- lists of posts with characteristics: `archives.html`, `period_archives.html`

- lists of objects: `authors.html`, `categories.html`, `tags.html`

- contents: `article.html`, `page.html`

The `base.html` template is special and does not belong to any group.

The templates in one group share as much code as possible.

In addition, some commonly used blocks are in `templates/blocks`.

## Tweaking

Clone or fork this repository and install the source theme with the `-s` instead of
the `-i` option to symlink to the version you downloaded, and hack at will.
The source stylesheets are not minified. If you fix a bug, please consider
sending a patch or a pull request. Thank you!
 
## Screenshots

![Author page](screenshot-author.png)

![Tags page](screenshot-tags.png)

## Copyright & License

Attilight theme is copyright (c) 2017 JC Bagneris - Released under the MIT License.

Attila theme is copyright (c) 2016 Arulraj V - Released under The MIT License.

