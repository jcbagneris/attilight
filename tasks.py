from invoke import task

BUILDDIR = "_build"
PREFIX = ".."
CSSPATH = "static/css"


@task(help={'builddir': "Temporary build directory, default: {0}".format(BUILDDIR)})
def clean(ctx, builddir=BUILDDIR):
    """Remove temporary build directory
    """
    ctx.run("rm -rf {0}".format(builddir), echo=True)


@task(
    pre=[clean],
    help={'builddir': "Temporary build directory, default: {0}".format(BUILDDIR)}
    )
def build(ctx, builddir=BUILDDIR):
    """Build css with postcss
    """
    params = {
            'builddir': builddir,
            'csspath': CSSPATH,
            }
    ctx.run("mkdir -p {builddir}".format(**params))
    for srcdir in ('templates', 'static'):
        ctx.run(
                "cp -a {srcdir} {builddir}/".format(srcdir=srcdir,**params),
                echo=True,
                )
    commands = (
        "source ~/virtualenvs/css/bin/activate",
        "postcss --use autoprefixer cssnano -d {builddir}/{csspath} {csspath}/*css"
        .format(**params),
        )
    ctx.run(" && ".join(commands), echo=True)
    for font in ('ttf','eot','svg','woff'):
        version = ctx.run(
            "sha256sum {builddir}/{csspath}/fonts/icons.{font} | cut -c-7"
            .format(font=font,**params),
            hide=True,
            ).stdout.strip()
        ctx.run(
            "mv {builddir}/{csspath}/fonts/icons.{font} {builddir}/{csspath}/fonts/icons-{version}.{font}"
            .format(font=font,version=version,**params),
            echo=True,
            )
        ctx.run(
            "sed -e 's/icons\.{font}/icons-{version}.{font}/g' -i {builddir}/{csspath}/style.css"
            .format(font=font,version=version,**params),
            echo=True,
            )
    for css in ('normalize','pygment','style'):
        version = ctx.run(
            "sha256sum {builddir}/{csspath}/{css}.css | cut -c-7"
            .format(css=css,**params),
            hide=True
            ).stdout.strip()
        ctx.run(
            "mv {builddir}/{csspath}/{css}.css {builddir}/{csspath}/{css}-{version}.css"
            .format(css=css,version=version,**params),
            echo=True,
            )
        ctx.run(
            "sed -e 's/{css}\.css/{css}-{version}.css/g' -i {builddir}/templates/base.html"
            .format(css=css,version=version,**params),
            echo=True,
            )



@task(
    pre=[build],
    help={
        'prefix': "Installation prefix, default: {0}".format(PREFIX),
        'builddir': "Temporary build directory, default: {0}".format(BUILDDIR),
        }
    )
def local(ctx, prefix=PREFIX, builddir=BUILDDIR):
    """Copy a built version of attilight locally
    """
    params = {
            'prefix': prefix,
            'builddir': builddir,
            }
    ctx.run("rm -rf {prefix}/attilight".format(**params))
    ctx.run("cp -a {builddir}/ {prefix}/attilight ".format(**params))


